<? require "templates/parts/header.php" ?>
<? require "templates/parts/breadcrumb.php" ?>
<? require_once "html.php" ?>
<table id="commits" class="box">
  <tr>
    <th>Commit</th>
    <th>Time</th>
    <th colspan="2">Author</th>
    <th>Message</th>
  </tr>
  <? foreach ($COMMITS as $commit) { ?>  
  <tr>
    <td class="hash">
      <a href="<? htmlText($commit->getCommitUrl()) ?>"><? htmlHash($commit->getCommitHash(), 7)?></a>
    </td>
    <td class="time">
      <? htmlTime($commit->getAuthorDate()) ?>
    </td>
    <td class="avatar">
      <? htmlAvatar($commit->getAuthor()) ?>
    </td>
    <td class="author">
      <? htmlContact($commit->getAuthor()) ?>
    </td>
    <td class="message">
      <div class="decouple"><div>
        <a href="<? htmlText($REPO->getObject($commit->getCommitHash(), $PATH)->getUrl()) ?>"><? htmlText($commit->getSubject()) ?></a>
      </div></div>
    </td>
  </tr>
  <?php } ?>
</table>
<? require "templates/parts/footer.php" ?>
