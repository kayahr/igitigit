<? $revision = $REVISION ? $REVISION : $REPO->getBranch() ?>
<div id="revision">
  Revision
  <span class="currentRevision">
    <? if (preg_match('/[a-z0-9]{40}/', $revision)) { ?>
      <? htmlHash($revision, 7) ?>
    <? } else { ?>
      <? htmlText($revision) ?>
    <? } ?>
  </span>
  <div class="revisions">
    <h1>Branches</h1>
    <ul class="branches">
      <? foreach ($REPO->getBranches() as $branch) { ?>
        <li><a href="<? htmlText($REPO->getObject($branch)->getUrl())?>"><? htmlText($branch) ?></a></li>
      <? } ?>
    </ul>
    <? $tags = $REPO->getTags() ?>
    <? if ($tags) { ?>
      <h1>Tags</h1>
      <ul class="tags">
        <? foreach ($tags as $tag) { ?>
          <li><a href="<? htmlText($REPO->getObject($tag)->getUrl())?>"><? htmlText($tag) ?></a></li>
        <? } ?>
      </ul>
    <? } ?>
  </div>
</div>
