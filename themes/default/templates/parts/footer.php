    </div>
    <div id="footer">
      <div class="content">
        <div class="admin">
          <a href="mailto:<? htmlText(ADMIN_EMAIL) ?>">Contact admin</a>
        </div>
        <div class="powered-by">
          Powered by <a href="http://github.com/kayahr/igitigit">igitigit</a>
          version <? htmlText(VERSION) ?>
        </div>
      </div>
    </div>
  </body>
</html>