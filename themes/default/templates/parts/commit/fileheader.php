<? require_once "html.php" ?>
<div id="diff-<?=$file->getIndex()?>" class="box">
  <div class="header">
    <ul class="actions">
      <li>
        <? htmlHash($file->getParentHash(), 7, $file->getParentBlobUrl()) ?>
        &#x2192;
        <? htmlHash($file->getCommitHash(), 7, $file->getCommitBlobUrl()) ?>
      </li>
    </ul>
    <ul class="info">
      <li><? htmlText($file->getFilename()) ?></li>
    </ul>
  </div>
  <? require $file->isBinary() ? "binaryheader.php" : "textheader.php" ?>
