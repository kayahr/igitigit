<? require_once "html.php" ?>
<div class="breadcrumb">
<? if (isset($COMMITS)) { ?>History for<? } ?>
<? $first = true; foreach ($BREADCRUMBS as $breadcrumb) { ?>
  <? if ($first) $first = false; else { ?> / <? } ?>
  <? if ($breadcrumb->isCurrent() && !isset($COMMITS)) { ?>
    <? htmlText($breadcrumb->getName()) ?>
  <? } else { ?>
    <a href="<? htmlText($breadcrumb->getUrl()) ?>"><? htmlText($breadcrumb->getName()) ?></a>
  <? } ?>
<? } ?>
</div>