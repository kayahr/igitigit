<? require_once "html.php" ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title><? if (isset($TITLE)) htmlText($TITLE . " - ") ?>igitigit</title>
    <style type="text/css">
      @import "<?=BASEURL?>/themes/<?=THEME?>/css/styles.css";
    </style>
  </head>
  <body>
    <div id="header">
      <div class="content">
        <h1><a href="http://github.com/kayahr/igitigit">igitigit</a></h1>
        <? if (isset($REPO)) { ?>
        <? require "templates/parts/revision.php" ?>
        <? } ?>
      </div>
    </div>
    <div id="content">
