<? require_once "html.php" ?>
<? if ($REPO) { ?>
<div class="commitheader">
  <? $commit = $REPO->getCommit($REVISION); ?>
  <table class="hashes">
    <tr>
      <th>Commit</th>
      <td><a href="<? htmlText($commit->getCommitUrl())?>"><? htmlHash($commit->getCommitHash(), 7) ?></a></td>
    </tr>
    <tr>
      <th>Tree</th>
      <td><a href="<? htmlText($commit->getTreeUrl())?>"><? htmlHash($commit->getTreeHash(), 7) ?></a></td>
    </tr>
    <tr>
      <th>Parent</th>
      <td><a href="<? htmlText($commit->getParentCommitUrl())?>"><? htmlHash($commit->getParentHash(), 7) ?></a></td>
    </tr>
  </table>
  <div class="subject"><? htmlText($commit->getSubject()) ?></div>
  <div class="contacts">
    <div class="contact-data author">
      <? htmlAvatar($commit->getAuthor()) ?>
      Author: <? htmlContact($commit->getAuthor()) ?><br />
      Time: <? htmlTime($commit->getAuthorDate()) ?>
    </div>
    <? if ($commit->getAuthor() != $commit->getCommitter()) { ?>
      <div class="contact-data committer">
        <? htmlAvatar($commit->getCommitter()) ?>
        Committer: <? htmlContact($commit->getCommitter()) ?><br />
        Time: <? htmlTime($commit->getCommitterDate()) ?>
      </div>
    <? } ?>
  </div>
</div>
<? } ?>