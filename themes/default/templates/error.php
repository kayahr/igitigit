<? header("HTTP/1.1 $ERROR_CODE $ERROR_MESSAGE") ?>
<? $TITLE = $ERROR_CODE ?>
<? require "templates/parts/header.php" ?>
<div class="error">
  <p>
    <? htmlText($ERROR_MESSAGE) ?>
  </p>
  <? if (DEBUG) { ?>
  <dl>
    <dt>Message</dt>
    <dd class="message"><? htmlText($ERROR->getMessage()) ?></dd>
    <dt>Location</dt>
    <dd class="location">
      <span class="file"><? htmlText($ERROR->getFile()) ?></span>
      line
      <span class="line"><? htmlText($ERROR->getLine()) ?></span>
    </dd>
    <dt>Type</dt>
    <dd class="type"><? htmlText(get_class($ERROR)) ?></dd>
    <dt>Code</dt>
    <dd class="code"><? htmlText($ERROR->getCode()) ?></dd>
    <dt>Stack trace</dt>
    <dd class="stackTrace"><? htmlText($ERROR->getTraceAsString()) ?></dd>
  </dl>
  <? } ?>
</div>
<? require "templates/parts/footer.php" ?>
