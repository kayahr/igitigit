<? require "templates/parts/header.php" ?>
<? require "templates/parts/commitheader.php" ?>
<? require "templates/parts/breadcrumb.php" ?>
<? require_once "html.php" ?>
<div id="blob" class="box">
  <div class="header">
    <ul class="actions">
      <li><a href="<? htmlText($FILE->getRawUrl()) ?>">Raw</a></li>
      <? if ($REPO) { ?>
        <? if (!$FILE->isBinary()) { ?>
          <li><a href="<? htmlText($FILE->getBlameUrl()) ?>">Blame</a></li>
        <? } ?>
        <li><a href="<? htmlText($FILE->getHistoryUrl()) ?>">History</a></li>
      <? } ?>
    </ul>
    <ul class="info">
      <li><? htmlFileMode($FILE->getMode()) ?></li>
      <li><? htmlFileSize($FILE->getSize()) ?></li>
    </ul>
  </div>
  <div class="body">
    <? 
    $mimeType = $FILE->getMimeType(); 
    if ($FILE->isImage())
        require "templates/parts/blob/image.php";
    else if ($mimeType == "text/x-web-markdown")
        require "templates/parts/blob/markdown.php";
    else
        require "templates/parts/blob/text.php";
    ?>
  </div>
</div>
<? require "templates/parts/footer.php" ?>
