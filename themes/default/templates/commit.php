<? require_once "html.php" ?>
<? require "templates/parts/header.php" ?>
<? require "templates/parts/commitheader.php" ?>
<? require "templates/parts/breadcrumb.php" ?>
<div id="commit">
  <? $renderer = new igitigit\CommitRenderer($REPO) ?>
  <? $renderer->render($REVISION) ?>
</div>
<? require "templates/parts/footer.php" ?>
