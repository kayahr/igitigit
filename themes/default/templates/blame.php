<? require "templates/parts/header.php" ?>
<? require "templates/parts/commitheader.php" ?>
<? require "templates/parts/breadcrumb.php" ?>
<? require_once "html.php" ?>
<div id="blame" class="box">
  <div class="header">
    <ul class="actions">
      <li><a href="<? htmlText($FILE->getRawUrl()) ?>">Raw</a></li>
      <? if ($REPO) { ?>
      <li><a href="<? htmlText($FILE->getUrl()) ?>">View</a></li>
      <li><a href="<? htmlText($FILE->getHistoryUrl()) ?>">History</a></li>
      <? } ?>
    </ul>
    <ul class="info">
      <li><? htmlFileMode($FILE->getMode()) ?></li>
      <li><? htmlFileSize($FILE->getSize()) ?></li>
    </ul>
  </div>
  <div class="body">
    
    <?/*
       * The blame-info and line-numbers cells are just dummies in this
       * table and are overwritten by the table which is rendered after
       * this one.
       */?>
    <table>
      <tr>  
        <td class="blame-info"></td>
        <td class="line-numbers"></td>
        <td class="text-file">
<pre class="text-file">
<? $blame = $FILE->dumpBlame() ?>
</pre>
        </td>
      </tr>
      
      <?/*
         * This table row is needed to size the line-numbers cell
         * correctly. The row is hidden in the browser.
         */?>
      <tr class="hide">
        <td></td>
        <td class="line-numbers">
          <div class="commit">
            <? htmlText($blame->getTotalLines()) ?>
          </div>
        </td>
        <td></td>
      </tr>
      
    </table>

    <?/*
       * This is the real table with the blame info and the line numbers
       * which is absolutely positioned on top of the fake table. This
       * hack is needed because the line numbers and blame info are
       * only known AFTER the file itself has been written. So to write
       * everything in the correct order we had to read all data twice or
       * cache the complete file in memory. Both is not a good idea for large
       * files and that's why this hack is used instea.
       */?>
    <table class="line-numbers">
      <tr>
        <td class="blame-info">
          <? foreach ($blame->getGroups() as $group) { $commit = $group->getCommit(); $lines = $group->getLines() ?>
            <div class="commit" style="height:<?=$group->getLines()?>em">
              <div class="commitData">
                <a href="<? htmlText($commit->getCommitUrl()) ?>"><? htmlHash($commit->getCommitHash(), 7) ?></a>
                <a title="Blame" class="blameLink" href="<? htmlText($commit->getBlameUrl())?>">&#x25b6;</a>
                <? htmlTime($commit->getAuthorDate(), "%Y-%m-%d") ?>
                <? htmlAvatar($commit->getAuthor()) ?>
                <? htmlContact($commit->getAuthor()) ?>
              </div>
              <div class="commitSummary">
                <? htmlText($commit->getSummary()) ?>
              </div>
            </div>
          <? } ?>
        </td>
        <td class="line-numbers">
          <? $line = 1; foreach ($blame->getGroups() as $group) { ?>
            <div class="commit">
              <? for ($i = $group->getLines(); $i > 0; $i--) { ?>
                <a href="#L<?=$line?>"><?=$line++?></a><br />
              <? } ?>
            </div>
          <? } ?>
        </td>
      </tr>
    </table>
  </div>
</div>
<? require "templates/parts/footer.php" ?>
