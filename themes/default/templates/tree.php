<? require_once "html.php" ?>
<? require "templates/parts/header.php" ?>
<? require "templates/parts/commitheader.php" ?>
<? require "templates/parts/breadcrumb.php" ?>
<table id="tree" class="box">
  <tr>  
    <th class="type"><span>Type</span></th>  
    <th class="name">Name</th>
    <th class="time">Time</th>
    <th class="author" colspan="2">Author</th>
    <th class="message">Message</th>
    <th class="actions">
      <? if ($REPO) { ?>
        <a href="<? htmlText($OBJECT->getHistoryUrl()) ?>">History</a>
      <? } ?>
    </th>
  </tr>
  <? foreach ($DIR->getObjects() as $object) { ?>
  <tr class="type-<? htmlText($object->getType()) ?>">
    <td class="type">
      <span><? htmlText($object->getType()) ?></span>
    </td>
    <td class="name">
      <a href="<? htmlText($object->getUrl()) ?>"><? htmlText($object->getName()) ?></a>
    </td>
    <td class="time">
      <? htmlTime($object->getLastModified()) ?>
    </td>
    <? if ($object instanceof igitigit\RepoObject) { $commit = $object->getLastCommit() ?>
      <td class="avatar">
        <? if ($commit) htmlAvatar($commit->getAuthor()) ?>
      </td>
      <td class="contact">
        <? if ($commit) htmlContact($commit->getAuthor()) ?>
      </td>
      <td colspan="2" class="message">
        <? if ($commit) { ?>
        <div class="decouple"><div>
          <a href="<? htmlText($commit->getCommitUrl()) ?>"><? htmlText($commit->getSubject()) ?></a>
        </div></div>
        <? } ?>
      </td>
    <? } else { ?>
      <td colspan="4" class="message"></td>
    <? } ?>
  </tr>
  <? } ?>
</table>
<? require "templates/parts/footer.php" ?>
