<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * Thrown when GIT execution fails.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
class GitException extends \Exception
{
    /**
     * Constructs the exception.
     *
     * @param string $cmd
     *            The executed command.
     * @param int $errorCode
     *            The returned error code.
     * @param string $errorFile
     *            The file containing the error message.
     */
    public function __construct($cmd, $errorCode, $errorFile)
    {
        $errorMessage = file_get_contents($errorFile);
        unlink($errorFile);
        $message = sprintf("GIT execution failed with error code %s.\n" .
            "Executed command: %s\nError message:\n%s", $errorCode, $cmd,
            $errorMessage);
        parent::__construct($message, $errorCode);
    }
}
