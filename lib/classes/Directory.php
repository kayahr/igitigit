<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * Directory interface.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
interface Directory extends Object
{
    /**
     * Returns the parent directory or NULL if this is the root directory.
     *
     * @return Directory
     *            The parent directory or NULL if root directory.
     */
    public function getParent();
    
    /**
     * Checks if this directory is the root directory.
     *
     * @return boolean
     *            True when this directory is the root direcotry, false if
     *            not.
     */
    public function isRoot();

    /**
     * Returns a child object from this directory. If no object with the
     * specified name was found then NULL is returned.
     *
     * @param string $name
     *            The file name.
     * @return Object
     *            The object or NULL if not found.
     */
    public function getObject($name);
    
    /**
     * Returns all objects from this directory. Returns an empty list if the
     * directory is empty. The returned list is sorted by type and
     * alphabetically.
     *
     * @return Object[]
     *            The list of files in this directory.
     */
    public function getObjects();
}
