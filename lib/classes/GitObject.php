<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * A GIT tree.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
abstract class GitObject implements RepoObject
{
    /** The GIT repository. */
    protected $repo;
    
    /** The revision. */
    protected $revision;
    
    /** The path. */
    protected $path;
    
    /** The cached last commit. */
    private $lastCommit = NULL;
    
    /**
     * Constructs a new GIT object.
     *
     * @param GitRepository $repo
     *            The GIT repository this object belongs to.
     * @param string $revision
     *            The revision.
     * @param string $path
     *            The tree path. The root tree must have an empty path.
     */
    public function __construct(GitRepository $repo, $revision, $path)
    {
        $this->repo = $repo;
        $this->revision = $revision;
        $this->path = $path;
    }
    
    /**
     * @see RepoObject#getLastCommit()
     *
     * @return Commit
     */
    final public function getLastCommit()
    {
        if (!$this->lastCommit)
        {
            $commits = $this->repo->getCommits($this->revision,
                $this->path, 1);
            $this->lastCommit = $commits[0];
        }
        return $this->lastCommit;
    }
    
    /**
     * @see Object#getLastModified()
     * 
     * @return int
     */
    final public function getLastModified()
    {
        return $this->getLastCommit()->getAuthorDate();
    }
}
