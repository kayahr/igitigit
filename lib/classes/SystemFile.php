<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * A system file.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
class SystemFile implements File, SystemObject
{
    /** The path (relative to REPOS). */
    private $path;
    
    /** The file name. */
    private $name;
    
    /** The parent file. May be null if directly in REPOS. */
    private $parent;
    
    /** The cached file mime type. */
    private $mimeType = NULL;

    /**
     * Constructs a new file.
     *
     * @param string $name
     *             The file name.
     * @param SystemDirectory $parent
     *             The parent directory
     */    
    public function __construct($name, SystemDirectory $parent)
    {
        $this->name = $name;
        $this->parent = $parent;

        // Calculate path
        $parentPath = $this->parent->getPath();
        $this->path = $parentPath;
        if ($parentPath && $name) $this->path .= "/";
        $this->path .= $name;
    }
    
    /**
     * @see Object#getName()
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @see Object#getPath()
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * @see Object#getMode()
     *
     * @return int
     */
    public function getMode()
    {
        $stats = stat($this->getAbsPath());
        return $stats["mode"];
    }

    /**
     * @see Object#getUrl()
     *
     * @return string
     */    
    public function getUrl()
    {
        $path = $this->getPath();
        return CONTROLLER . ($path ? "/$path" : "");
    }
    
    /**
     * @see Object#getType()
     *
     * @return string
     */
    public function getType()
    {
        return Object::TYPE_SYSTEM_FILE;
    }
    
    /**
     * @see Object#getLastModified()
     * 
     * @return int
     */
    public function getLastModified()
    {
        return filemtime($this->getAbsPath());
    }

    /**
     * @see File#getSize()
     *
     * @return int
     */    
    public function getSize()
    {
        return filesize($this->getAbsPath());
    }

    /**
     * @see File#getParent()
     *
     * @return SystemDirectory
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    /**
     * @see File#getBreadcrumbs()
     * 
     * @return Breadcrumb[]
     */
    public function getBreadcrumbs($skipCurrent = true)
    {
        $breadcrumbs = array();
        $current = $this;
        while ($current)
        {
            array_unshift($breadcrumbs, new Breadcrumb($current->getName(),
                $current->getUrl(), $skipCurrent && $current == $this));
            $current = $current->getParent();
        }
        return $breadcrumbs;
    }
    
    /**
     * @see File#getMimeType()
     *
     * @return string
     */
    public function getMimeType()
    {
        if (!$this->mimeType)
        {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_file($finfo, $this->getAbsPath());
            finfo_close($finfo);
           
            // Correct mime type of markdown files.
            if ($mimeType == "text/plain" &&
                preg_match("/.*\\.md\$/", $this->getName()))
            {
                $mimeType = "text/x-web-markdown";
            }
            
            $this->mimeType = $mimeType;
        }
        return $this->mimeType;
    }

    /**
     * @see File#dump()
     *
     * @return int
     */
    public function dump()
    {
        $lines = 0;
        $file = fopen($this->getAbsPath(), "rt");
        while ($line = fgets($file))
        {
            $lines++;
            printf("<span id=\"line-%d\" class=\"line\">%s</span>",
                $lines, htmlspecialchars($line));
        }
        fclose($file);
        return $lines;
    }
    
    /**
     * @see File#dumpRaw()
     */
    public function dumpRaw()
    {
        $file = fopen($this->getAbsPath(), "rb");
        while ($line = fgets($file))
        {
            echo $line;
        }
    }

    /**
     * @see File#getContent()
     *
     * @return string
     */
    public function getContent()
    {
        return file_get_contents($this->getAbsPath());
    }

    /**
     * @see Object#getRawUrl()
     *
     * @return string
     */    
    public function getRawUrl()
    {
        $path = $this->getPath();
        return CONTROLLER . ($path ? "/$path" : "") . "/raw";
    }

    /**
     * @see SystemObject#getAbsPath()
     *
     * @return string
     */    
    public function getAbsPath()
    {
        $path = $this->getPath();
        return REPOS . ($path ? ("/" . $this->getPath()) : "");
    }
    
    /**
     * @see File#isBinary()
     *
     * @return boolean
     */    
    public function isBinary()
    {
        return !preg_match("/^text\\/.*/", $this->getMimeType());
    }

    /**
     * @see File#isImage()
     *
     * @return boolean
     */    
    public function isImage()
    {
        return preg_match("/^image\\/.*/", $this->getMimeType());
    }
}
