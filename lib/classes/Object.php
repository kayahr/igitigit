<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * File interface.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
interface Object
{
    /** A repository. */
    const TYPE_REPO = "repo";
    
    /** A directory in a repository. */
    const TYPE_REPO_DIR = "repo-dir";
    
    /** A file in a repository. */
    const TYPE_REPO_FILE = "repo-file";
    
    /** A system directory. */
    const TYPE_SYSTEM_DIR = "system-dir";
    
    /** A system file. */
    const TYPE_SYSTEM_FILE = "system-file";
    
    /**
     * Returns the object name.
     *
     * @return string
     *            The object name.
     */
    public function getName();
    
    /**
     * Returns the object path relative to REPOS. The path never has
     * a trailing or leading slash.
     *
     * @return string
     *            The object path relative to REPOS.
     */
    public function getPath();
    
    /**
     * Returns the object mode as decimal.
     *
     * @return int
     *            The object mode.
     */
    public function getMode();

    /**
     * Returns the last-modified timestamp.
     * 
     * @return int
     *            The last-modified timestamp.
     */
    public function getLastModified();

    /**
     * Returns the URL to access this object in the browser. The URL never
     * has a trailing slash.
     *
     * @return string
     *            The URL to access this object.
     */    
    public function getUrl();

    /**
     * Returns the breadcrumbs.
     * 
     * @param boolean $skipCurrent
                  If linking the current path should be skipped. Defaults to
                  true.
     * @return Breadcrumb[]
     *            The breadcrumbs.
     */
    public function getBreadcrumbs($skipCurrent = true);
    
    /**
     * Returns the object type. This is one of the OBJECT::TYPE_*
     * constants.
     *
     * @return string
     *            The object type.
     */
    public function getType();
}
