<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * A GIT file.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
class GitFile extends GitObject implements File
{
    /** The parent node. */
    private $parent = false;
    
    /** The file name. */
    private $name;
    
    /** The file mode. */
    private $mode;
    
    /** The size. */
    private $size;

    /** The cached file mime type. */
    private $mimeType = NULL;
    
    /**
     * Constructs a new GIT tree.
     *
     * @param GitRepository $repo
     *            The GIT repository this tree belongs to.
     * @param string $revision
     *            The revision.
     * @param string $path
     *            The tree path. The root tree must have an empty path.
     * @param int $mode
     *            The file mode.
     * @param int $size
     *            The file size.
     */
    public function __construct(GitRepository $repo, $revision, $path,
        $mode, $size)
    {
        parent::__construct($repo, $revision, $path);
        $this->name = basename($path);
        $this->mode = $mode;
        $this->size = $size;
    }
    
    /**
     * @see Object@getName()
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @see Object#getPath()
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * @see Object#getMode()
     * 
     * @return int
     */
    public function getMode()
    {
        return $this->mode;
    }
    
    /**
     * @see Object#getUrl()
     *
     * @return string
     */
    public function getUrl()
    {
        $repo = $this->repo;
        $revision = $this->revision;
        $path = $this->path;
        $repoUrl = $this->repo->getUrl();
        return "$repoUrl/blob/$revision/$path";
    }

    /**
     * @see Object#getBreadcrumbs()
     * 
     * @return Breadcrumb[]
     */
    public function getBreadcrumbs($skipCurrent = true)
    {
        $breadcrumbs = array();
        $current = $this;
        while ($current)
        {
            $name = $current->getName();
            if (!$name) $name = $current->getRepo()->getName();
            array_unshift($breadcrumbs, new Breadcrumb($name,
                $current->getUrl(), $skipCurrent && $current == $this));
            $current = $current->getParent();
        }
        return $breadcrumbs;
    }
    
    /**
     * @see Object#getType()
     *
     * @return string
     */
    public function getType()
    {
        return Object::TYPE_REPO_FILE;
    }
    
    /**
     * @see RepoObject#getRevision()
     *
     * @return string
     */
    public function getRevision()
    {
        return $this->revision;
    }
        
    /**
     * @see RepoObject#getRepo()
     *
     * @return GitRepository
     */
    public function getRepo()
    {
        return $this->repo;
    }

    /**
     * @see RepoObject#getHistoryUrl()
     *
     * @return string
     */
    public function getHistoryUrl()
    {
        $path = $this->path;
        return $this->repo->getHistoryURL() . ($path ? "/$path" : "");
    }

    /**
     * @see RepoObject#getCommits()
     *
     * @return Commit[]
     */
    public function getCommits()
    {
        return $this->repo->getCommits(NULL, $this->path);
    }

    /**
     * @see Object#getParent()
     *
     * @return GitDirectory
     */    
    public function getParent()
    {
        // Return parent from cache if possible
        if ($this->parent !== false) return $this->parent;
        
        // Determine parent
        $parentPath = dirname($this->path);
        if ($parentPath == ".") $parentPath = "";
        $parent = $this->repo->getObject($this->revision, $parentPath);
            
        // Cache and return the parent
        $this->parent = $parent;
        return $this->parent;
    }
    
    /**
     * @see File#getSize()
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }
    
    /**
     * @see File#getMimeType()
     *
     * @return string
     */
    public function getMimeType()
    {
        if (!$this->mimeType)
        {
            $blob = $this->repo->getBlob($this->revision, $this->path);
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeType = finfo_buffer($finfo, $blob);
            finfo_close($finfo);

            // Correct mime type of markdown files.
            if ($mimeType == "text/plain" &&
                preg_match("/.*\\.md\$/", $this->getName()))
            {
                $mimeType = "text/x-web-markdown";
            }
            
            $this->mimeType = $mimeType;
        }
        return $this->mimeType;
    }

    /**
     * @see File#dump()
     *
     * @return int
     */
    public function dump()
    {
        return $this->repo->dumpBlob($this->revision, $this->path);
    }
    
    /**
     * @see File#dumpRaw()
     */
    public function dumpRaw()
    {
        $this->repo->dumpRawBlob($this->revision, $this->path);
    }
    
    /**
     * @see File#getContent()
     *
     * @return string
     */
    public function getContent()
    {
        return $this->repo->getBlob($this->revision, $this->path);
    }

    /**
     * Dumps the lines of a blame output (All lines grouped per commit in
     * a single div) and returns the blame information for each commit.
     *
     * @return Blame[]
     */
    public function dumpBlame()
    {
        return $this->repo->dumpBlame($this->revision, $this->path);
    }

    /**
     * @see File#getRawUrl()
     *
     * @return string
     */
    public function getRawUrl()
    {
        $repo = $this->repo;
        $revision = $this->revision;
        $path = $this->path;
        $repoUrl = $this->repo->getUrl();
        return "$repoUrl/raw/$revision/$path";
    }

    /**
     * Returns the blame URL.
     *
     * @return string
     *             The blame URL
     */
    public function getBlameUrl()
    {
        $repo = $this->repo;
        $revision = $this->revision;
        $path = $this->path;
        $repoUrl = $this->repo->getUrl();
        return "$repoUrl/blame/$revision/$path";
    }

    /**
     * @see File#isBinary()
     *
     * @return boolean
     */    
    public function isBinary()
    {
        return !preg_match("/^text\\/.*/", $this->getMimeType());
    }

    /**
     * @see File#isImage()
     *
     * @return boolean
     */    
    public function isImage()
    {
        return preg_match("/^image\\/.*/", $this->getMimeType());
    }
}
