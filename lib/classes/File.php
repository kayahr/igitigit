<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * File interface.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
interface File extends Object
{
    /**
     * Returns the file size in bytes.
     *
     * @return int
     *            The file size.
     */    
    public function getSize();

    /**
     * Returns the parent directory.
     *
     * @return Directory
     *            The parent directory.
     */
    public function getParent();
    
    /**
     * Returns the mime type of this file.
     *
     * @return string
     *            The mime type of this file.
     */
    public function getMimeType();
    
    /**
     * Checks if file is a binary file.
     *
     * @return boolean
     *            True if file is binary, false if not.
     */
    public function isBinary();
 
    /**
     * Checks if file is an image.
     *
     * @return boolean
     *            True if file is an image, false if not.
     */   
    public function isImage();
    
    /**
     * Dumps the content of this file to the HTML output. Special 
     * characters are encoded but line-breaks are not touched so the caller
     * must wrap the output with a pre tag.
     *
     * @return int
     *            The number of printed lines or NULL if no line numbers are
     *            available.
     */
    public function dump();
    
    /**
     * Dumps the raw content of this file to the HTML output.
     */
    public function dumpRaw();
    
    /**
     * Returns the content of this file.
     *
     * @return string
     *            The file content.
     */
    public function getContent();
    
    /**
     * Returns the URL to the raw representation of this file. The URLs never 
     * have a trailing slash.
     *
     * @return string
     *            The raw file URL.
     */
    public function getRawUrl();
}
