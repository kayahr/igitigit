<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * A container for blame information.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
class Blame
{
    /** The repository. */
    private $repo;
    
    /** The path of the file. */
    private $path;
    
    /** The seen commits. */
    private $commits = array();
    
    /** The blame groups. */
    private $groups = array();
    
    /** The current commit. */
    private $commit;
    
    /** The current blame group. */
    private $group;
    
    /** The total number of lines. */
    private $lines = 0;
    
    /**
     * Constructs a new blame object.
     *
     * @param Repository $repo
     *            The repository.
     * @param string $path
     *            The path of the file.
     */
    public function __construct(Repository $repo, $path)
    {
        $this->repo = $repo;
        $this->path = $path;
    }
    
    /**
     * Returns the repository.
     *
     * @return Repository
     *            The repository.
     */
    public function getRepo()
    {
        return $this->repo;
    }
    
    /**
     * Returns the path of the file.
     *
     * @return string
     *            The path of the file.
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * Sets the commit..
     *
     * @param string $hash
     *            The commit hash.
     * @param int $lines
     *            The number of lines.
     * @return boolean
     *            True if a new commit was opened, false if commit has
     *            not changed.
     */
    public function setCommit($hash, $lines)
    {
        // Do nothing if commit has not changed
        if ($this->commit && $this->commit->getCommitHash() == $hash)
        {
            $this->group->addLines($lines);
            return false;
        }
            
        // Use already known commit or create a new one
        if (isset($this->commits[$hash]))
        {
            $this->commit = $this->commits[$hash];
        }
        else
        {
            $this->commit = new BlameCommit($this, $hash);
            $this->commits[$hash] = $this->commit;
        }
        
        // Create new blame group.
        $this->group = new BlameGroup($this->commit, $lines);
        $this->groups[] = $this->group;
        
        // Increment total number of lines
        $this->lines += $lines;
        return true;
    }
    
    /**
     * Sets the current author name.
     *
     * @param string $authorName
     *            The author name to set.
     */
    public function setAuthorName($authorName)
    {
        $this->commit->setAuthorName($authorName);
    }
    
    /**
     * Sets the current author email address.
     *
     * @param string $authorEMail
     *            The author email address to set.
     */
    public function setAuthorEMail($authorEMail)
    {
        $this->commit->setAuthorEMail($authorEMail);
    }

    /**
     * Sets the current author date (Unix timestamp).
     *
     * @param int $authorDate
     *            The author date to set.
     */
    public function setAuthorDate($authorDate)
    {
        $this->commit->setAuthordate($authorDate);
    }
    
    /**
     * Sets the summary.
     *
     * @param string $summary
     *            The summary to set.
     */
    public function setSummary($summary)
    {
        $this->commit->setSummary($summary);
    }
    
    /**
     * Returns the total number of lines.
     *
     * @return number
     *            The total number of lines.
     */
    public function getTotalLines()
    {
        return $this->lines;
    }
    
    /**
     * Returns the blame groups.
     *
     * @return The blame groups.
     */
    public function getGroups()
    {
        return $this->groups;
    }
}
