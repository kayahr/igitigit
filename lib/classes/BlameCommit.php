<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * Blame info about a commit.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
class BlameCommit
{
    /** The blame object. */
    private $blame;
    
    /** The commit hash. */
    private $commitHash;
    
    /** The author date (Unix timestamp). */
    private $authorDate;
    
    /** the author name. */
    private $authorName;

    /** the author email. */
    private $authorEMail;

    /** The summary. */    
    private $summary;
    
    /**
     * Constructs a new commit blame info.
     *
     * @param Blame $blame
     *            The blame object.
     * @param string $commitHash
     *            The commit hash.
     */
    public function __construct(Blame $blame, $commitHash)
    {
        $this->blame = $blame;
        $this->commitHash = $commitHash;
    }
    
    /**
     * Returns the commit hash.
     *
     * @return string
     *            The commit hash.
     */     
    public function getCommitHash()
    {
        return $this->commitHash;
    }
    
    /**
     * Returns the short abbreviated commit hash.
     *
     * @return string
     *            The abbreviated commit hash.
     */
    public function getShortCommitHash()
    {
        return substr($this->commitHash, 0, 8);
    }
    
    /**
     * Sets the author date.
     *
     * @param int $authorDate
     *                The author date to set.
     */
    public function setAuthorDate($authorDate)
    {
        $this->authorDate = $authorDate;
    }
    
    /**
     * Returns the author date (Unix timestamp).
     *
     * @return int
     *            The author date
     */
    public function getAuthorDate()
    {
        return $this->authorDate;
    }
    
    /**
     * Sets the author name.
     *
     * @param string $authorName
     *           The author name to set.
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
    }

    /**
     * Sets the author email.
     *
     * @param string $authorEMail
     *           The author email to set.
     */
    public function setAuthorEMail($authorEMail)
    {
        $this->authorEMail = $authorEMail;
    }

    /**
     * Returns the author.
     *
     * @return Contact
     *            The author.
     */
    public function getAuthor()
    {
        return new Contact($this->authorName, $this->authorEMail);
    }
    
    /**
     * Sets the summary.
     *
     * @param string $summary
     *            The summary to set.
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;
    }

    /**
     * Returns the summary.
     *
     * @retrun string
     *            The summary.
     */     
    public function getSummary()
    {
        return $this->summary;
    }
    
    /**
     * Returns the URL to this commit.
     *
     * @return string
     *            The commit URL.
     */
    public function getCommitUrl()
    {
        return $this->blame->getRepo()->getURL() . "/commit/" .
            $this->commitHash;
    }

    /**
     * Returns the blame URL of this commit.
     *
     * @return string
     *            The blame URL.
     */
    public function getBlameUrl()
    {
        return $this->blame->getRepo()->getURL() . "/blame/" .
            $this->commitHash . "/" . $this->blame->getPath();
    }
}
