<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * A contact.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
class Contact
{
    /** The name. */
    private $name;
    
    /** The email address. */
    private $email;
    
    /**
     * Constructs a new contact.
     *
     * @param string $name
     *            The name.
     * @param string $email
     *            The email address.
     */
    public function __construct($name, $email)
    {
        $this->name = $name;
        $this->email = $email;
    }
    
    /**
     * Returns the name.
     *
     * @return string
     *            The name.
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Returns the email address.
     *
     * @return string
     *            The email address.
     */
    public function getEMail()
    {
        return $this->email;
    }
}
