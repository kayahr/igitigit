<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * Info about a commit.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
class Commit
{
    /** The repository this commit belongs to. */
    private $repo;
    
    /** The commit hash. */
    private $commitHash;
    
    /** The tree hash. */
    private $treeHash;
    
    /** The parent hash. */
    private $parentHash;
    
    /** The author date (Unix timestamp). */
    private $authorDate;
    
    /** the author. */
    private $author;

    /** The committer date (Unix timestamp). */    
    private $committerDate;
    
    /** The committer. */
    private $committer;

    /** The subject. */    
    private $subject;
    
    /**
     * Constructs a new commit info.
     *
     * @param Repository $repo
     *            The repo this commit belongs to.
     * @param string $commitHash
     *            The commit hash.
     * @param string $treeHash
     *            The tree hash.
     * @param string $parentHash
     *            The parent hash.
     * @param int $authorDate
     *            The author date.
     * @param Contact $author
     *            The author.
     * @param int $committerDate
     *            The committer date.
     * @param Contact $commiter
     *            The committer.
     * @param string $subject
     *            The subject.
     */
    public function __construct(Repository $repo, $commitHash, $treeHash,
        $parentHash, $authorDate, Contact $author, $committerDate,
        Contact $committer, $subject)
    {
        $this->repo = $repo;
        $this->commitHash = $commitHash;
        $this->treeHash = $treeHash;
        $this->parentHash = $parentHash;
        $this->authorDate = $authorDate;
        $this->author = $author;
        $this->committerDate = $committerDate;
        $this->committer = $committer;
        $this->subject = $subject;
    }
    
    /**
     * Returns the commit hash.
     *
     * @return string
     *            The commit hash.
     */     
    public function getCommitHash()
    {
        return $this->commitHash;
    }
    
    /**
     * Returns the tree hash.
     *
     * @return string
     *            The tree hash.
     */
    public function getTreeHash()
    {
        return $this->treeHash;
    }
    
    /**
     * Returns the parent hash.
     *
     * @return string
     *            The parent hash.
     */
    public function getParentHash()
    {
        return $this->parentHash;
    }
    
    /**
     * Returns the author date (Unix timestamp).
     *
     * @return int
     *            The author date
     */
    public function getAuthorDate()
    {
        return $this->authorDate;
    }

    /**
     * Returns the author.
     *
     * @return Contact
     *            The author.
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Returns the committer date (Unix timestamp).
     *
     * @return int
     *            The committer date.
     */    
    public function getCommitterDate()
    {
        return $this->committerDate;
    }
    
    /**
     * Returns the committer.
     *
     * @return Contact
     *            The committer.
     */
    public function getCommitter()
    {
        return $this->committer;
    }

    /**
     * Returns the subject.
     *
     * @retrun string
     *            The subject.
     */     
    public function getSubject()
    {
        return $this->subject;
    }
    
    /**
     * Returns the URL to this commit.
     *
     * @return string
     *            The commit URL.
     */
    public function getCommitUrl()
    {
        return $this->repo->getURL() . "/commit/" . $this->commitHash;
    }
    
    /**
     * Returns the URL to the tree of this commit.
     *
     * @return string
     *            The tree URL.
     */
    public function getTreeUrl()
    {
        return $this->repo->getURL() . "/tree/" . $this->commitHash;
    }
    
    /**
     * Returns the URL to the parent commit.
     *
     * @return string
     *            The parent commit URL.
     */
    public function getParentCommitUrl()
    {
        return $this->repo->getURL() . "/commit/" . $this->parentHash;
    }
}
