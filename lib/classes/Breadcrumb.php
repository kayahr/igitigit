<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * A breadcrumb.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
class Breadcrumb
{
    /** The breadcrumb name. */
    private $name;
    
    /** The breadcrumb URL. */
    private $url;
    
    /** If this is the current breadcrumb or not. */
    private $current;
    
    /**
     * Constructs a new breadcrumb.
     *
     * @param string $name
     *            The breadcrumb name.
     * @param string $url
     *            The breadcrumb url.
     * @param boolean $current
     *            If this is the current breadcrumb or not.
     */
    public function __construct($name, $url, $current)
    {
        $this->name = $name;
        $this->url = $url;
        $this->current = $current;
    }
    
    /**
     * Returns the breadcrumb name.
     *
     * @return string
     *            The breadcrumb name.
     */     
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Returns the breadcrumb URL.
     *
     * @return string
     *            The breadcrumb URL.
     */     
    public function getUrl()
    {
        return $this->url;
    }
    
    /**
     * Checks if this breadcrumb is the current one.
     *
     * @return boolean
     *            True if this breadcrumb is the current one, false if not.
     */
    public function isCurrent()
    {
        return $this->current;
    }
}
