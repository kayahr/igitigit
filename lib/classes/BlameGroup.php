<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * Blame group.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
class BlameGroup
{
    /** The commit. */
    private $commit;
    
    /** The number of lines in this group. */
    private $lines;
    
    /**
     * Constructs a new blame group.
     *
     * @param BlameCommit $commit
     *            The blame object.
     * @param int $lines
     *            The number of lines in this group.
     */
    public function __construct(BlameCommit $commit, $lines)
    {
        $this->commit = $commit;
        $this->lines = $lines;
    }
    
    /**
     * Returns the commit.
     *
     * @return BlameCommit
     *            The commit.
     */     
    public function getCommit()
    {
        return $this->commit;
    }
    
    /**
     * Returns the number of lines.
     *
     * @return string
     *            The number of lines.
     */
    public function getLines()
    {
        return $this->lines;
    }
    
    /**
     * Adds lines to this blame group.
     *
     * @param int $lines
     *            The number of lines to add.
     */
    public function addLines($lines)
    {
        $this->lines += $lines;
    }
}
