<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * Repository interface.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
interface Repository extends RepoObject
{
    /**
     * Returns the list of branches available in this repository.
     *
     * @return string[]
     *             The branch list.
     */
    public function getBranches();

    /**
     * Checks if this repository has a branch with the specified name.
     *
     * @param string $branch
     *            The branch name to check.
     * @return boolean
     *            True if repo has this branch, false if nto.
     */    
    public function hasBranch($branch);

    /**
     * Returns the current branch.
     *
     * @return string
     *            The current branch. Null if there is no branch.
     */    
    public function getCurrentBranch();

    /**
     * Returns the selected branch. This defaults to the current
     * branch but can be overriden with setBranch() which remembers the
     * selection in the HTTP session.
     *
     * @return string
     *            The selected branch.
     */
    public function getBranch();

    /**
     * Sets the selected branch.
     *
     * @param string $branch    
     *            The selected branch to set.
     */
    public function setBranch($branch);

    /**
     * Returns the list of tags available in this repository.
     *
     * @return string[]
     *             The tag list.
     */
    public function getTags();

}
