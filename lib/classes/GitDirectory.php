<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

/**
 * A GIT tree.
 *
 * @author Klaus Reimer <k@ailis.de>
 */
final class GitDirectory extends GitObject implements Directory
{
    /** The parent node. */
    private $parent = false;
    
    /** The tree name. */
    private $name;
    
    /** The directory mode. */
    private $mode;
    
    /**
     * Constructs a new GIT tree.
     *
     * @param GitRepository $repo
     *            The GIT repository this tree belongs to.
     * @param string $revision
     *            The revision.
     * @param string $path
     *            The tree path. The root tree must have an empty path.
     * @param int $mode
     *            The directory mode.
     */
    public function __construct(GitRepository $repo, $revision, $path, $mode)
    {
        parent::__construct($repo, $revision, $path);
        $this->name = basename($path);
        $this->mode = $mode;
    }
    
    /**
     * @see Object@getName()
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * @see Object#getPath()
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    
    /**
     * @see Object#getMode()
     * 
     * @return int
     */
    public function getMode()
    {
        return $this->mode;
    }
    
    /**
     * @see Object#getUrl[B()
     *
     * @return string
     */
    public function getUrl()
    {
        $repo = $this->repo;
        $revision = urlencode($this->revision);
        $path = $this->path;
        $repoUrl = $this->repo->getUrl();
        if (!$path && $revision == $repo->getBranch()) return $repoUrl;
        return "$repoUrl/tree/$revision" . ($path ? "/$path" : "");
    }

    /**
     * @see Object#getBreadcrumbs()
     * 
     * @return Breadcrumb[]
     */
    public function getBreadcrumbs($skipCurrent = true)
    {
        $breadcrumbs = array();
        $current = $this;
        while ($current)
        {
            $name = $current->getName();
            if (!$name) $name = $current->getRepo()->getName();
            array_unshift($breadcrumbs, new Breadcrumb($name,
                $current->getUrl(), $skipCurrent && $current == $this));
            $current = $current->getParent();
        }
        return $breadcrumbs;
    }
    
    /**
     * @see Object#getType()
     */
    public function getType()
    {
        return Object::TYPE_REPO_DIR;
    }
    
    /**
     * @see RepoObject#getRevision()
     *
     * @return string
     */
    public function getRevision()
    {
        return $this->revision;
    }
        
    /**
     * @see RepoObject#getRepo()
     *
     * @return GitRepository
     */
    public function getRepo()
    {
        return $this->repo;
    }

    /**
     * @see RepoObject#getHistoryUrl()
     *
     * @return string
     */
    public function getHistoryUrl()
    {
        $path = $this->path;
        return $this->repo->getHistoryURL() . ($path ? "/$path" : "");
    }

    /**
     * @see RepoObject#getCommits()
     *
     * @return Commit[]
     */
    public function getCommits()
    {
        return $this->repo->getCommits(NULL, $this->path);
    }

    /**
     * @see Directory#getParent()
     *
     * Returns either another GIT directory or the parent directory of the
     * repository if this is the root GIT directory.
     *
     * @return string
     */    
    public function getParent()
    {
        // Return parent from cache if possible
        if ($this->parent !== false) return $this->parent;
        
        // Determine parent
        if (!$this->path)
            $parent = $this->repo->getParent();
        else
        {
            $parentPath = dirname($this->path);
            if ($parentPath == ".") $parentPath = "";
            $parent = $this->repo->getObject($this->revision, $parentPath);
        }
            
        // Cache and return the parent
        $this->parent = $parent;
        return $this->parent;
    }

    /**
     * @see Directory#isRoot()
     *
     * @return boolean
     */
    public function isRoot()
    {
        return $this->getParent() == NULL;
    }
    
    /**
     * @see Directory#getObject()
     *
     * @param string $name
     * @return RepoObject
     */     
    public function getObject($name)
    {
        // TODO Implement me
        return NULL;
    }
    
    /**
     * @see Directory#getObjects()
     *
     * @return RepoObject[]
     */
    public function getObjects()
    {
        return $this->repo->getObjects($this->revision, $this->path);
    }
}
