<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Outputs a time. The HTML construct is a span tag with class name "time"
 * which contains the abbreviated time. The full time is set as title on
 * the span tag.
 *
 * @param string $format
 *            Optional custom format.
 * @param int $timestamp
 *            The UNIX timestamp
 */
function htmlTime($timestamp, $format = "%b %d, %Y")
{
    $fullTime = strftime("%Y-%m-%d %H:%M:%S", $timestamp);
    $abbrTime = strftime($format, $timestamp);
    printf("<span class=\"time\" title=\"%s\">%s</span>",
        htmlspecialchars($fullTime),
        htmlspecialchars($abbrTime));
}

/**
 * Prints simple text. The maximum length can be specified. If the string
 * is longer than this length then it is trimmed down to this length and
 * three dots are prepended to it. When the text was shortened then it
 * is wrapped in a span tag which has the full text as title.
 *
 * @param string $text
 *            The text to print.
 * @param int $maxLen
 *            The maximum length of the text to print.
 */
function htmlText($text, $maxLen = NULL)
{
    if ($maxLen && strlen($text) > $maxLen)
    {
        $title = htmlspecialchars($text);
        $content = htmlspecialchars(substr($text, 0, $maxLen - 3) . "...");
    }
    else
    {
        $title = NULL;
        $content = htmlspecialchars($text);
    }
    if ($title)
        printf("<span title=\"%s\">%s</span>", $title, $content);
    else
        echo $content;
}

/**
 * Prints a contact. The constract is a mailto-link with the class name
 * 'contact'. The content of the link is the contact name.
 *
 * @param Contact $contact
 *            The contact.
 */
function htmlContact(igitigit\Contact $contact)
{
    printf("<a class=\"contact\" href=\"mailto:%s\">%s</a>",
        htmlspecialchars($contact->getEMail()),
        htmlspecialchars($contact->getName()));
};

/**
 * Prints the avatar of a contact.
 * 
 * @param Contact $contact
 *            The contact.
 * @param int $size
 *            Optional avatar image size to request (Defaults to 140).
 */
function htmlAvatar(igitigit\Contact $contact, $size = 140)
{
    $hash = md5($contact->getEMail());
    $secure = false; // TODO Auto-detect
    $url = $secure ? "https://secure.gravatar.com" : "http://gravatar.com";
    $url .= "/avatar/$hash?s=$size";
    echo "<a class=\"gravatar\" href=\"http://www.gravatar.com/$hash\">";
    echo "<img src=\"$url\" alt=\"\" />";
    echo "</a>";
}

/**
 * Prints a file mode.
 *
 * @param int $mode
 *            The file mode in decimal.
 */
function htmlFileMode($mode)
{
    $types = array(
        001 => "p",
        002 => "c",
        006 => "b",
        010 => "-",
        014 => "s",
        040 => "d",
        041 => "l"
    );
    $modeString = sprintf("%s%s%s%s%s%s%s%s%s%s",
        isset($types[$mode >> 12]) ? $types[$mode >> 12] : "?",
        $mode & 0400 ? 'r' : '-',
        $mode & 0200 ? 'w' : '-',
        $mode & 0100 ? ($mode & 04000 ? 's' : 'x') : ($mode & 04000 ? 'S' : '-'),
        $mode & 0040 ? 'r' : '-',
        $mode & 0020 ? 'w' : '-',
        $mode & 0010 ? ($mode & 02000 ? 's' : 'x') : ($mode & 02000 ? 'S' : '-'),
        $mode & 0004 ? 'r' : '-',
        $mode & 0002 ? 'w' : '-',
        $mode & 0001 ? ($mode & 01000 ? 't' : 'x') : ($mode & 01000 ? 'T' : '-')
    );

    printf("<span class=\"filemode\" title=\"File mode %o\">%s</span>", $mode, $modeString);
}

/**
 * Prints a file size.
 *
 * @param int $size
 *            The file size in bytes.
 */
function htmlFileSize($size)
{
    $fileSize = number_format($size);
    printf("<span title=\"File size\">%s bytes</span>", $fileSize);
}

/**
 * Prints a hash with the specified size. If no size is specified then the
 * full hash is printed. If the hash is shortened then the title attribute
 * is set to the full hash.
 *
 * @param string $hash
 *            The hash to print.
 * @param int $size
 *            Optional hash size.
 */
function htmlHash($hash, $size = 40, $url = NULL)
{
    if ($url) printf("<a href=\"%s\">", $url);
    if ($size >= 40)
        printf("<span class=\"hash\">%s</span>", $hash);
    else
        printf("<span class=\"hash\" title=\"%s\">%s</span>",
            $hash, substr($hash, 0, $size));
    if ($url) echo "</a>";
}
