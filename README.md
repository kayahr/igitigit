igitigit
========

Description
-----------

igitigit is a web frontend for Git repositories. It is easy to install and
easy to use. Only requirement is PHP 5.3 or higher.

igitigit can handle multiple repositories and doesn't care about 
directory structures because it simply let you browse directories the same
way as it let you browse repositories.


Installation
------------

Place all files somewhere in your web root and copy config.sample.php to
config.php and change the configuration values as needed. Then simply call
the controller PHP file (igitigit.php).


Implemented features
--------------------

- Directory browser
- Repository tree browser
- Tree history
- File/Blob viewer (Text only)
- Raw file/blob download
- Blob history
- Blame
- Commit viewer
- Branch selection
- Tag selection


TODO
----

- Syntax-highlighting in text viewer.
- All the other stuff I haven't thought about yet.
