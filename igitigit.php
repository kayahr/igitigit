<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

// The version
define("VERSION", "1.0.0");

// Set maximum error level
error_reporting(E_ALL);

// Start output buffering
ob_start();

// Start the session
session_start();

// Load configuration (It's ok if it is missing)
require_once "config.php";

// Configure error reporting
ini_set("display_errors", DEBUG ? 1 : 0);
ini_set("log_errors", 1);

// Auto-calculate BASEURL if not specified
if (!defined("BASEURL"))
    define("BASEURL", "." . str_repeat("/..",
        substr_count(realpath(dirname($_SERVER["SCRIPT_FILENAME"])),
        DIRECTORY_SEPARATOR) - substr_count(dirname(__FILE__), 
        DIRECTORY_SEPARATOR) + (isset($_SERVER["PATH_INFO"]) ?
        substr_count($_SERVER["PATH_INFO"], "/") : 0)));

// Auto-calculate the CONTROLLER url if not defined
if (!defined("CONTROLLER")) define("CONTROLLER", BASEURL . "/igitigit");

// Set the include path
set_include_path(
    dirname(__FILE__) . "/lib" . PATH_SEPARATOR .
    dirname(__FILE__) . "/themes/". THEME . "/" . PATH_SEPARATOR .
    dirname(__FILE__) . "/themes/default/" . PATH_SEPARATOR .
    get_include_path());
    
/**
 * Autoloads classes.
 *
 * @param className
 *            The name of the class to load.
 */
function autoLoadClass($className)
{
    $className = preg_replace('/.*\\\\/', "", $className);
    require_once "classes/$className.php";
}
spl_autoload_register("igitigit\\autoLoadClass");

/**
 * Handles an exception.
 *
 * @param Exception $exception
 *            The exception to handle.
 */
function handleException($exception)
{
    restore_exception_handler();
    ob_end_clean();
    $ERROR = $exception;
    if ($exception instanceof HttpException)
    {
        $ERROR_CODE = $exception->getCode();
        $ERROR_MESSAGE = $exception->getMessage();
        require "templates/error.php";
    }
    else
    {
        $ERROR_CODE = 500;
        $ERROR_MESSAGE = "An unknown exception occured";
        error_log($exception);
        require "templates/error.php";
    }
    exit;
}
set_exception_handler("igitigit\\handleException");

/**
 * Converts an error into an exception.
 *
 * @param int $level
 *            The error level.
 * @param string $message
 *            The error message.
 * @param string $file
 *            The file in which the error occured.
 * @param int $line
 *            The line where the error occured.
 * @param array $context
 *            The error context.
 */
function handleError($level, $message, $file = NULL, $line = NULL,
    array $context = NULL)
{
    throw new \ErrorException($message, 0, $level, $file, $line);
}
set_error_handler("igitigit\\handleError");

/**
 * Redirects the browser to the specified target. This function does not
 * return.
 *
 * @param target
 *            The target to redirect to.
 */
function redirect($target)
{
    header("Location: $target");
    exit;
}

// Parse request
$OPTIONS = explode("/", trim(isset($_SERVER["PATH_INFO"]) ?
    $_SERVER["PATH_INFO"] : "", "/"));

// Parse the request path. After this OBJECT is the latest system file,
// system directory or repository encountered in the path right before the
// command.  FILE is the system file (if any), DIR is the last system
// directory and REPO is the repository (if any).  OPTIONS now only contains
// the command and the options.
$OBJECT = $DIR = new SystemDirectory("Projects");
$FILE = NULL;
$REPO = NULL;
$DIR = NULL;
while (!$REPO && !$FILE && $OPTIONS)
{
    $OBJECT = $OBJECT->getObject(array_shift($OPTIONS));
    if (!$OBJECT)
        throw new HttpException("No such file or directory", 404);
    else if ($OBJECT instanceof File)
        $FILE = $OBJECT;
    else if ($OBJECT instanceof Repository)
        $REPO = $OBJECT;
    else
        $DIR = $OBJECT;
}

// Parse command from request. After this COMMAND is the requested command
// and OPTIONS only contain the options.
$COMMAND = array_shift($OPTIONS);

// If no command was specified then auto-detect it.
if (!$COMMAND)
{
    if ($FILE)
        $COMMAND = "blob";
    else
        $COMMAND = "tree";
}

// Validate command.
if (!preg_match('/^[a-z]+$/', $COMMAND))
    throw new HttpException("Invalid command specified", 400);
if (!file_exists("commands/$COMMAND.php"))
    throw new HttpException("Command not found", 404);

// Pass control over to the command and handle errors.
require "commands/$COMMAND.php";
