<?php
/*
 * igitigit - Web frontend for Git repositories
 * Copyright (C) 2011  Klaus Reimer <k@ailis.de>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace igitigit;

// Absolute path of the repositories root directory.
define("REPOS", "/srv/git");

// The base URL of the web. Instead of this complicated auto-calculating
// formula you can simply specify a fixed path matching your environment.
// define("BASEURL", "/igitigit");

// The URL to the igitigit.php script. Auto-calculated if not specified.
// define("CONTROLLER", "/igitigit/igitigit");

// The path to the git binary. If just "git" is specified without path then
// it is searched in the system PATH.
define("GIT", "git");

// The theme to use.
define("THEME", "default");

// Controls if detailed error messages should be displayed
define("DEBUG", false);

// The admin email address (Will be displayed in the footer)
define("ADMIN_EMAIL", "not@configured.tld");
